public class Window {
    public Window() {
        magneticSensor = new MagneticSensor();
        state = state.close;
    }
    {
        id = nextId++;
    }
    public void open() {
        state = state.open;
    }
    public void close() {
        state = state.close;
    }
    public String getHeader(){
        return "w"+id;
    }
    public int getState(){
        return state;
    }
    private MagneticSensor magneticSensor;
    private State state;
    private final int id;
    private static int nextId=0;
}
