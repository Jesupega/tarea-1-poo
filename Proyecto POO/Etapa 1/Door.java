public class Door {
    public Door () {
        magneticSensor = new MagneticSensor();
        state = state.CLOSE;
    }
    {
        id = nextId++;
    }
    public void open() {
        state = state.open;
    }
    public void close() {
        state = state.close;
    }
    public String getHeader(){
        return "d"+id;
    }
    public int getState(){
        return state;
    }

    private MagneticSensor magneticSensor;
    private State state;
    private final int id;
    private static int nextId;
    static {
        nextId = 0;
    }
}
