public class MagneticSensor extends Sensor {
    public MagneticSensor(){}
    public void moveMagnetAwayFromSwitch() {
        setState(OPEN);
    }
    public void putMagnetNearSwitch() {
        setState(CLOSE);
    }
}
